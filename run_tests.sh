
#!/bin/bash
# run_tests.sh

# Install dependencies
pip install -r requirements.txt

# Run tests
pytest --maxfail=1 --disable-warnings -v
